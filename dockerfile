FROM debian:9 as build
ARG LUAJIT_LIB=/usr/local/LuaJIT/lib
ARG LUAJIT_INC=/usr/local/LuaJIT/include/luajit-2.1
RUN apt-get update -y && apt-get install wget build-essential libpcre++-dev -y
WORKDIR /usr/local
RUN wget https://github.com/openresty/lua-nginx-module/archive/refs/tags/v0.10.15.tar.gz \
        && tar xvfz v0.10.15.tar.gz
RUN wget https://github.com/vision5/ngx_devel_kit/archive/refs/tags/v0.3.1.tar.gz \
        && tar xvzf v0.3.1.tar.gz
RUN wget http://luajit.org/download/LuaJIT-2.1.0-beta3.tar.gz \
        && tar xvfz LuaJIT-2.1.0-beta3.tar.gz \
        && cd LuaJIT-2.1.0-beta3 \
        && make install PREFIX=/usr/local/LuaJIT
RUN wget https://github.com/openresty/lua-resty-core/archive/refs/tags/v0.1.17.tar.gz \
        && tar xvfz v0.1.17.tar.gz \
        && mkdir -p /usr/local/share/lua/5.1/resty \
        && cp -r lua-resty-core-0.1.17/lib/resty /usr/local/share/lua/5.1/
RUN wget https://github.com/openresty/lua-resty-lrucache/archive/refs/tags/v0.10.tar.gz \
        && tar xvfz v0.10.tar.gz \
        && cp -r lua-resty-lrucache-0.10/lib/resty /usr/local/share/lua/5.1/
RUN wget http://nginx.org/download/nginx-1.19.3.tar.gz \
        && tar xvfz nginx-1.19.3.tar.gz \
        && mv nginx-1.19.3 nginx \
        && cd nginx \
        && ./configure \
                --prefix=/opt/nginx \
                --without-http_gzip_module \
                --with-ld-opt=-Wl,-rpath,/usr/local/LuaJIT/lib \
                --add-module=/usr/local/ngx_devel_kit-0.3.1 \
                --add-module=/usr/local/lua-nginx-module-0.10.15 \
                && make -j2 && make install
RUN rm -rf /opt/nginx/html/index.html
COPY ./index.html /opt/nginx/html/
RUN echo +
FROM debian:9
COPY --from=build /usr/local/LuaJIT /usr/local/LuaJIT
COPY --from=build /usr/local/ngx_devel_kit-0.3.1 /usr/local/ngx_devel_kit-0.3.1
COPY --from=build /usr/local/lua-nginx-module-0.10.15 /usr/local/lua-nginx-module-0.10.15
COPY --from=build /usr/local/share /usr/local/share
WORKDIR /opt/nginx/
COPY --from=build /opt/nginx/ .
WORKDIR /opt/nginx/sbin
RUN echo "daemon off;" >> /opt/nginx/conf/nginx.conf
ENTRYPOINT ["/bin/bash", "-ec", "./nginx"]
